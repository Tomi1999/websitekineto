import { Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { HeaderComponent } from './components/header/header.component';
import { AdminGuard, AuthGuard, Logged } from './guards/auth-guard.guard';
import { JwtModuleOptions } from '@auth0/angular-jwt';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { NewconsultComponent } from './components/newconsult/newconsult.component';
import { HistoryComponent } from './components/history/history.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ServicesComponent } from './components/services/services.component';
import { AdminComponent } from './components/admin/admin/admin.component';
import { UsersComponent } from './components/admin/users/users.component';
import { ServiceComponent } from './components/admin/services/services.component';
import { ConsultsComponent } from './components/admin/consults/consults.component';
import { MessageComponent } from './components/admin/contact/contact.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent,canActivate: [Logged]},
  { path:'profile', component: ProfileComponent, canActivate : [AuthGuard]},
  { path:'history', component: HistoryComponent, canActivate : [AuthGuard]},
  { path:'newconsult', component: NewconsultComponent, canActivate : [AuthGuard]},
  { path:'admin',
    children: [
      {
        path: 'user-management',
        component: UsersComponent
      },
      {
        path: 'service-management',
        component: ServiceComponent
      },
      {
        path: 'consult-management',
        component: ConsultsComponent
      },
      {
        path: 'contact-management',
        component: MessageComponent
      },
      { path: '', component: AdminComponent }
    ],
     canActivate : [AdminGuard]},
  { path:'contact', component: ContactComponent, canActivate : [AuthGuard]},
  { path:'services', component: ServicesComponent, canActivate : [AuthGuard]},
  { path: '', component: NotfoundComponent, canActivate: [AuthGuard]},
  { path:'home', component: HomeComponent, canActivate : [AuthGuard]},
  { path: 'register', component: RegisterComponent, canActivate: [Logged]},
  { path: '**', component: NotfoundComponent, canActivate: [AuthGuard]}
];






