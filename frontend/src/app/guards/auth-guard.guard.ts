import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivateFn } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { AdminButtonService, HeaderStateService } from '../services/header-state.service';

export const AuthGuard: CanActivateFn = () => {
  const userService = inject(UserService);
  const router = inject(Router);
  const header = inject(HeaderStateService)
  const adminBtn = inject(AdminButtonService)
    if (userService.isAuthenticated()) {
      header.show()
      if(userService.isAdmin())
        adminBtn.show()
      else
        adminBtn.hide()
      return true;
    }
    router.navigate(['login']);
    header.hide()
    return false;
}
export const Logged: CanActivateFn = () => {
  const userService = inject(UserService);
  const router = inject(Router);
  if(userService.isAuthenticated()){
    router.navigate(['home']);
    return false
  }
  return true
}

export const AdminGuard: CanActivateFn = () => {
  const userSerivce = inject(UserService)
  const router = inject(Router);
  if(userSerivce.isAuthenticated()){
    if(userSerivce.isAdmin()){
      return true
    }
    else{
      router.navigate(['home'])
      return false
    }
  }
  router.navigate(['login'])
  return false
}

