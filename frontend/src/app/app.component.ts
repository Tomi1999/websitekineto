import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderStateService } from './services/header-state.service';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { apiConfig } from './config/api.config';
import { environment } from '../environments/environments';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from './interceptor/token.interceptor';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,HeaderComponent,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'frontend';
  constructor(public headerState : HeaderStateService) { 
    this.headerState.headerEvent.subscribe((visible : boolean) => this.headerState.visible = visible);
  }
  
}
