import { Entity } from "./entity";
import { User, UserAccount, UserEmail } from "./user";

export class Consult{
    id : number;
    user: UserEmail;
    date: Date;
    service: string;
}
