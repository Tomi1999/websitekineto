import { Entity } from "./entity";

export class Service {
    name: string;
    price: number;
}
export class ServiceEntity extends Entity{
    name: string;
    price: number;
}