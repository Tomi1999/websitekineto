import { Entity } from "./entity";

export class Note {
    title: string;
    message: string;
    email:string;
}
export class NoteEntity extends Entity{
    title: string;
    message: string;
    email:string;
}