import { Entity } from "./entity";

export class User{
    email: string;
    password: string;
}
export class UserAccount extends User{
    id: number;
    isAdmin : boolean;
}
export class UserEmail{
    id:number
    email:string
}
