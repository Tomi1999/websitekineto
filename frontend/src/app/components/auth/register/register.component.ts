import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIcon } from '@angular/material/icon';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { HeaderStateService } from '../../../services/header-state.service';
import { UserService } from '../../../services/user.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, ReactiveFormsModule,MatCardModule,MatIcon,MatButton,MatButtonModule,CommonModule]
})
export class RegisterComponent {
  profileForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$")]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  url = "../../../assets/Tom.png";
  message = {name:'', color:'red'}
  constructor(public headerState : HeaderStateService, private userSerivce : UserService, private router : Router) {
    this.headerState.hide();
   }
  onSubmit() {
    if(!this.profileForm.valid) {
      this.url="../../../assets/Tom2.png";
      this.message.name ='invalid input'
    }
    else{
    this.userSerivce.register(this.profileForm.get('email')?.value!, this.profileForm.get('password')?.value!).subscribe({
      next:(data) => {

      if(data.message === "Already registered") {
        this.url="../../../assets/Tom2.png";
        this.message.name=data.message
        this.message.color="red"
      }
      else{
        this.url="../../../assets/Tom.png";
        this.message.name=data.message
        this.message.color="green"
      }
    },
    error: (error) => {
      this.url="../../../assets/Tom2.png";
      this.message.name="failed"
      this.message.color="red"
    }
  })
    }
  }
  ngOnInit(): void {
    
  }

}
