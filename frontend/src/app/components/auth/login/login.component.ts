import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormControl, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatIcon} from '@angular/material/icon';
import {MatButton, MatButtonModule} from '@angular/material/button';
import { HeaderStateService } from '../../../services/header-state.service';
import { UserService } from '../../../services/user.service';
import { TokenService } from '../../../services/token.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, ReactiveFormsModule,MatCardModule,MatIcon,MatButton,MatButtonModule,CommonModule]
})
export class LoginComponent {
  
  profileForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  //login.ts
  url = "../../../assets/Tom.png";
  message = {name:'', color:'red'}
  constructor(public headerState : HeaderStateService, private userSerivce : UserService, private tokenService : TokenService, private router : Router) {
    this.headerState.hide();
   }
   onSubmit() {
    if(!this.profileForm.valid) {
      this.url="../../../assets/Tom2.png";
      this.message.name = 'Invalid input'
    }
    this.userSerivce.login(this.profileForm.get('email')?.value!, this.profileForm.get('password')?.value!).subscribe({
      next: (data) => {

      if(data.token != null) {
        this.tokenService.setToken(data.token);
        this.router.navigate(['home']);
        this.headerState.show();
      }
      else {
        this.message.name = "Wrong email or password"
        this.url="../../../assets/Tom2.png";
      }
    },
    error: (error) => {
      this.message.name = "Unknown error"
      this.url="../../../assets/Tom2.png";
    }
  })
  }

  
  ngOnInit(): void {
    
  }

}
