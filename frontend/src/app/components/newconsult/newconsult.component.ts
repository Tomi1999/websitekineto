import { Component, Inject } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Consult } from '../../models/conult';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RestRequestService } from '../../services/rest-request.service';
import {MatSelectModule} from '@angular/material/select';
import { User, UserEmail } from '../../models/user';
import { CommonModule } from '@angular/common';
import { Service } from '../../models/service';
import { MatOptionModule } from '@angular/material/core';

@Component({
  selector: 'app-newconsult',
  standalone: true,
  imports: [MatCardModule,MatFormFieldModule, MatInputModule,MatDatepickerModule,ReactiveFormsModule,CommonModule],
  templateUrl: './newconsult.component.html',
  styleUrl: './newconsult.component.scss'
})
export class NewconsultComponent {
  hours: { time: string, status: string }[] = [
    { time: '9:00', status: 'available' },
    { time: '10:00', status: 'available' },
    { time: '11:00', status: 'available' },
    { time: '12:00', status: 'available' },
    { time: '13:00', status: 'available' },
    { time: '14:00', status: 'available' },
    { time: '15:00', status: 'available' },
    { time: '16:00', status: 'available' },
    { time: '17:00', status: 'available' },
    { time: '18:00', status: 'available' },
    { time: '19:00', status: 'available' },
    { time: '20:00', status: 'available' }
  ];
  dateControl:FormControl;
  constructor(public dialog: MatDialog, private restRequestService : RestRequestService<Consult> ){
    this.dateControl = new FormControl(new Date());
    this.dateControl.valueChanges.subscribe(newValue => {
      this.updateHourStatuses(newValue);
    });
  }
  ngOnInit(): void {
    this.updateHourStatuses(this.dateControl.value)
  }
  updateHourStatuses(selectedDate: Date) {
    const currentDate = new Date();
  
    // Obține consultațiile din backend și așteaptă finalizarea cererii
    this.restRequestService.get("/api/consults/future").subscribe((consults: any[]) => {
      this.hours.forEach(hour => {
        const [hourStr, minuteStr] = hour.time.split(':');
        const selectedDateTime = new Date(selectedDate);
        selectedDateTime.setHours(parseInt(hourStr, 10), parseInt(minuteStr, 10));
  
        // Verifică dacă ora selectată este înainte de data curentă
        if (selectedDateTime < currentDate) {
          hour.status = 'unavailable';
        } else {
          // Verifică dacă există orice consultație pentru ora selectată
          const consultExist = consults.some(consult => {
            const consultDate = new Date(consult.date);
            return (
              consultDate.getFullYear() === selectedDate.getFullYear() &&
              consultDate.getMonth() === selectedDateTime.getMonth() &&
              consultDate.getDate() === selectedDateTime.getDate() &&
              consultDate.getHours() === selectedDateTime.getHours() &&
              consultDate.getMinutes() === selectedDateTime.getMinutes()
            );
          });
  
          // Setează starea butonului în funcție de existența unei consultații
          hour.status = consultExist ? 'unavailable' : 'available';
        }
      });
    });
  }
  isDisabled(status: string): boolean {
    return status === 'unavailable';
  }
getStatusColor(status: string): string {
  switch(status) {
    case 'available':
      return 'green';
    case 'unavailable':
      return 'red';
    default:
      return 'gray';
  }
}

  addConsult(hour : string, date : Date) {
    this.dialog.open(ConsultDialog, {
      
      data: {
        hour: hour,
        date: date,
      }
    }).afterClosed().subscribe(() => this.updateHourStatuses(this.dateControl.value));
  }

}

@Component({
  selector: 'newconsult.dialog',
  standalone: true,
  imports: [CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule],
  templateUrl: 'newconsult.dialog.html',
  styleUrls: ['./newconsult.dialog.scss']
})
export class ConsultDialog {

  selected: string = ''; 
  selectControl = new FormControl('', Validators.required); 
  services: Service[] = []; 

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { hour: string; date: Date },
    private restRequestService: RestRequestService<Consult>,
    public dialogRef: MatDialogRef<ConsultDialog>
  ) {}

  ngOnInit(): void {
    this.loadServices();
  }
  loadServices(): void {
    this.restRequestService.get('/api/service').subscribe((data: Service[]) => {
      this.services = data;
    });
  }
  cancel(){
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.selectControl.invalid) {
      // Dacă nu este selectat un serviciu, marchează controlul ca invalid
      this.selectControl.markAsTouched();
      return;
    }
  
    const consult = new Consult();
    consult.service = this.selected; // Serviciul selectat
    consult.date = this.data.date;
  
    // Setăm ora din `data.hour`
    const [hour, minutes] = this.data.hour.split(':').map(Number);
    consult.date.setHours(hour);
    consult.date.setMinutes(minutes);
  
    this.restRequestService.get('/api/protected-route').subscribe((user) => {
      const usr = new UserEmail();
      usr.id = user.userId;
      usr.email = user.email;
      consult.user = usr;
  
      console.log(consult);
      this.restRequestService.post(consult, '/api/consults/').subscribe(() => {
        this.dialogRef.close();
      });
    });
  }

}

