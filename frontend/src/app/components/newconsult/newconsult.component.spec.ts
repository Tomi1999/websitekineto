import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewconsultComponent } from './newconsult.component';

describe('NewconsultComponent', () => {
  let component: NewconsultComponent;
  let fixture: ComponentFixture<NewconsultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NewconsultComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NewconsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
