import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RestRequestService } from '../../services/rest-request.service';
import { Note } from '../../models/note';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [MatCardModule,MatLabel,ReactiveFormsModule,MatButtonModule,MatFormFieldModule, MatInputModule],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss'
})
export class ContactComponent {

  constructor(public restRequestService: RestRequestService<Note>){}

  noteForm = new FormGroup({
    title: new FormControl('',[Validators.required,Validators.minLength(1)]),
    message: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });

  onSubmit(){
    if(this.noteForm.valid){
    const note = new Note();
    note.message = this.noteForm.get('message')?.value!
    note.title = this.noteForm.get('title')?.value!
    this.restRequestService.get("/api/protected-route").subscribe(user => {
      note.email = user.email
      this.restRequestService.post(note, "/api/notes").subscribe()
      this.noteForm.get('message')?.setValue("");
      this.noteForm.get('title')?.setValue("");
    })
    }
  }



}
