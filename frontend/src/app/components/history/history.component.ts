import { Component } from '@angular/core';
import { RestRequestService } from '../../services/rest-request.service';
import { User, UserAccount } from '../../models/user';
import { Consult } from '../../models/conult';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-history',
  standalone: true,
  imports: [MatCardModule,CommonModule],
  templateUrl: './history.component.html',
  styleUrl: './history.component.scss'
})
export class HistoryComponent {
  constructor(public restRequestService: RestRequestService<Consult>){}
  ngOnInit(): void {
    this.showConsults();
  }

  public oldConsults : Consult[] = [];
  public futureConsults : Consult[] = []

  showConsults(){
    const currentDate = new Date();
    let consults : Consult[]
    this.restRequestService.get("/api/myconsults").subscribe(consult => {
      this.futureConsults = []
      this.oldConsults = []
      consults = consult;
      consults.sort((a, b) => {
        const dateA = new Date(a.date).getTime();
        const dateB = new Date(b.date).getTime();
        return dateA - dateB;
      });
      consults.forEach(consult => {
        const consultDate = new Date(consult.date)
        if (consultDate < currentDate) {
          this.oldConsults.push(consult);
        } else {
          this.futureConsults.push(consult);
        }
      });
    }
  )
  }

  async deleteConsult(consult : Consult){
    await this.restRequestService.delete("/api/myconsults/" + consult.id).subscribe(() =>{
      this.showConsults()
    })
  }

}
