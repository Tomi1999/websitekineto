import { Component } from '@angular/core';
import { RestRequestService } from '../../services/rest-request.service';
import { User, UserAccount } from '../../models/user';
import { MatCard, MatCardModule, MatCardTitle } from '@angular/material/card';
import { map } from 'rxjs';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [MatCardModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  constructor() { }

  ngOnInit(): void {
   
  }

}
