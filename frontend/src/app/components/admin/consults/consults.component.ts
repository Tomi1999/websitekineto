import { Component, Inject, inject, OnInit } from '@angular/core';
import { RestRequestService } from '../../../services/rest-request.service';
import { User, UserAccount } from '../../../models/user';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { MatInputModule } from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Consult } from '../../../models/conult';
import { Service, ServiceEntity } from '../../../models/service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-consults',
  standalone: true,
  imports: [MatDatepickerModule,MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatNativeDateModule,
    FormsModule,MatCardModule,CommonModule,MatLabel,MatFormFieldModule,ReactiveFormsModule],
  templateUrl: './consults.component.html',
  styleUrl: './consults.component.scss'
})
export class ConsultsComponent {consults: Consult[] = []; 
  filteredConsults: Consult[] = []; 

  selectedDate: string | null = null; 
  userSearchQuery: string = ''; 
  selectedService: string | null = null; 

  services: string[] = [];

  constructor(private restRequestService: RestRequestService<Consult>) {}

  ngOnInit() {
    this.showConsults();
  }

  showConsults() {
    this.restRequestService.get("/api/consults/future").subscribe((response: any) => {
      this.consults = response.map((item: any) => {
        const consult = new Consult();
        consult.id = item.id;
        consult.service = item.service;
        consult.date = new Date(item.date);
        consult.user = item.user;
        return consult;
      });
      this.consults.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
      this.services = [...new Set(this.consults.map(consult => consult.service))];
      this.filteredConsults = [...this.consults];
    });
  }
  deleteConsult(consult : Consult){
    this.restRequestService.delete("/api/consults/" + consult.id).subscribe(() =>{
      this.showConsults()
    })
  }
  filterConsults() {
    this.filteredConsults = this.consults
      .filter(consult => {
        if (this.selectedDate) {
          const selectedDateStart = new Date(this.selectedDate);
          selectedDateStart.setHours(0, 0, 0, 0);
          const selectedDateEnd = new Date(this.selectedDate);
          selectedDateEnd.setHours(23, 59, 59, 999);
          const consultDate = new Date(consult.date);
          if (!(consultDate >= selectedDateStart && consultDate <= selectedDateEnd)) {
            return false;
          }
        }
        if (this.userSearchQuery) {
          const query = this.userSearchQuery.toLowerCase();
          if (!consult.user.email.toLowerCase().includes(query)) {
            return false;
          }
        }
        if (this.selectedService && consult.service !== this.selectedService) {
          return false;
        }

        return true; 
      })
      .sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime()); 
  }
}