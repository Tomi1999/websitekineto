import { Component, Inject, inject, OnInit } from '@angular/core';
import { RestRequestService } from '../../../services/rest-request.service';
import { User, UserAccount } from '../../../models/user';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { MatInputModule } from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Consult } from '../../../models/conult';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [MatCardModule,CommonModule,MatLabel,MatFormFieldModule,ReactiveFormsModule],
  templateUrl: './users.component.html',
  styleUrl: './users.component.scss'
})
export class UsersComponent {
  constructor(public dialog: MatDialog,public restRequestService: RestRequestService<User>, public userService: UserService){}
  ngOnInit(): void {
    this.showUsers();
  }
  userForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.minLength(1)]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  public users : UserAccount[]
  showUsers(){
    this.restRequestService.get("/api/users").subscribe(user => {
      this.users = user
    })
  }
  deleteUser(user: UserAccount){
    this.restRequestService.delete("/api/users/" + user.id).subscribe(() =>{
      this.showUsers()
    })
  }
  editUser(user: UserAccount){
    this.dialog.open(UserDialog, {
      data: {id : user.id, email : user.email, isAdmin: user.isAdmin, password: user.password}
    }).afterClosed().subscribe(() => this.showUsers())
  }
  showHistoryUser(user: UserAccount){
    this.dialog.open(UserHistoryDialog, {
      data: {id : user.id, email : user.email, isAdmin: user.isAdmin, password: user.password}
    })
  }
  onSubmit(){
    if(this.userForm.valid){
    const user = new User();
    user.email = this.userForm.get('email')?.value!
    user.password = this.userForm.get('password')?.value!
    this.userService.register(this.userForm.get('email')?.value!, this.userForm.get('password')?.value!).subscribe(() =>{
      this.userForm.get('email')?.setValue("");
      this.userForm.get('password')?.setValue("");
      this.showUsers()
    })
    }
  }
}


@Component({
  selector: 'users.dialog',
  standalone: true,
  imports: [MatCardModule,MatFormFieldModule,MatInputModule,MatFormFieldModule,ReactiveFormsModule,MatCheckboxModule],
  templateUrl: 'users.dialog.html',
  styleUrls: ['./users.dialog.scss']
})
export class UserDialog {
  userForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.minLength(1)]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
    isAdmin: new FormControl(false)
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: UserAccount,
  private restRequestService : RestRequestService<User>,
  public dialogRef: MatDialogRef<UserDialog>) {
    this.userForm.get('email')?.setValue(data.email);
    this.userForm.get('password')?.setValue(data.password);
    this.userForm.get('isAdmin')?.setValue(data.isAdmin); 

  }

  cancel(){
    this.dialogRef.close();
  }
  onSubmit(){
    if(this.userForm.valid){
      const user = new UserAccount()
      user.email = this.userForm.get('email')?.value!
      user.password = this.userForm.get('password')?.value!
      user.isAdmin = this.userForm.get('isAdmin')?.value!
      this.restRequestService.put(user, "/api/users/" + this.data.id).subscribe();
      this.dialogRef.close()
    }
  }


}
@Component({
  selector: 'userhistory.dialog',
  standalone: true,
  imports: [MatCardModule,CommonModule],
  templateUrl: 'userhistory.dialog.html',
  styleUrls: ['./userhistory.dialog.scss']
})
export class UserHistoryDialog implements OnInit{

  public userHistory: Consult[] = [];  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: UserAccount,
    private restRequestService: RestRequestService<Consult>,
    public dialogRef: MatDialogRef<UserHistoryDialog>
  ) {}
  ngOnInit(): void {
    this.loadUserHistory();
  }
  public oldConsults : Consult[] = [];
  public futureConsults : Consult[] = []

  loadUserHistory(){
    const currentDate = new Date();
    let consults : Consult[]
    this.restRequestService.get("/api/consults/user/" + this.data.id).subscribe(consult => {
      this.futureConsults = []
      this.oldConsults = []
      consults = consult;
      consults.sort((a, b) => {
        const dateA = new Date(a.date).getTime();
        const dateB = new Date(b.date).getTime();
        return dateA - dateB;
      });
      consults.forEach(consult => {
        const consultDate = new Date(consult.date)
        if (consultDate < currentDate) {
          this.oldConsults.push(consult);
        } else {
          this.futureConsults.push(consult);
        }
      });
    }
  )
  }

  cancel(){
    this.dialogRef.close();
  }
}