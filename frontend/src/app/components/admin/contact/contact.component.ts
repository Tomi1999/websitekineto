import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { RestRequestService } from '../../../services/rest-request.service';
import { Note, NoteEntity } from '../../../models/note';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [MatCardModule,CommonModule,MatLabel],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss'
})
export class MessageComponent {
constructor(public dialog: MatDialog,public restRequestService: RestRequestService<Note>){}
  ngOnInit(): void {
    this.showMesages();
  }
  public notes : NoteEntity[]
  showMesages(){
    this.restRequestService.get("/api/notes").subscribe(note => {
      this.notes = note
    })
  }
  replyToUser(email: string) {
    const subject = encodeURIComponent("Răspuns la solicitarea ta");
    const body = encodeURIComponent("Bună ziua,\n\nVă răspundem cu privire la solicitarea dumneavoastră...");
    window.location.href = `mailto:${email}?subject=${subject}&body=${body}`;
  }
  deleteService(note: NoteEntity){
    this.restRequestService.delete("/api/notes/" + note.id).subscribe(() =>{
      this.showMesages()
    })
  }
}
