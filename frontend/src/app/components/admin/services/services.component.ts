import { Component, Inject, inject, OnInit } from '@angular/core';
import { RestRequestService } from '../../../services/rest-request.service';
import { User, UserAccount } from '../../../models/user';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { MatInputModule } from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Consult } from '../../../models/conult';
import { Service, ServiceEntity } from '../../../models/service';

@Component({
  selector: 'app-services',
  standalone: true,
  imports: [MatCardModule,CommonModule,MatLabel,MatFormFieldModule,ReactiveFormsModule],
  templateUrl: './services.component.html',
  styleUrl: './services.component.scss'
})
export class ServiceComponent {
  constructor(public dialog: MatDialog,public restRequestService: RestRequestService<Service>){}
  ngOnInit(): void {
    this.showServices();
  }
  serviceForm = new FormGroup({
    name: new FormControl('',[Validators.required,Validators.minLength(1)]),
    price: new FormControl(0, [Validators.required, Validators.min(0)])
  });
  public services : ServiceEntity[]
  showServices(){
    this.restRequestService.get("/api/service").subscribe(service => {
      this.services = service
    })
  }
  deleteService(service: ServiceEntity){
    this.restRequestService.delete("/api/service/" + service.id).subscribe(() =>{
      this.showServices()
    })
  }
  editService(service: ServiceEntity){
    this.dialog.open(ServiceDialog, {
      data: {id : service.id, name : service.name, price: service.price}
    }).afterClosed().subscribe(() => this.showServices())
  }
  onSubmit(){
    if(this.serviceForm.valid){
    const service = new Service();
    service.name = this.serviceForm.get('name')?.value!
    service.price = this.serviceForm.get('price')?.value!
    this.restRequestService.post(service, "/api/service").subscribe()
    this.serviceForm.get('name')?.setValue("");
    this.serviceForm.get('price')?.setValue(0);
    this.showServices();
    }
  }
}

@Component({
  selector: 'services.dialog',
  standalone: true,
  imports: [MatCardModule,MatFormFieldModule,MatInputModule,MatFormFieldModule,ReactiveFormsModule,MatCheckboxModule],
  templateUrl: 'services.dialog.html',
  styleUrls: ['./services.dialog.scss']
})
export class ServiceDialog {
  serviceForm = new FormGroup({
    name: new FormControl('',[Validators.required,Validators.minLength(1)]),
    price: new FormControl(0, [Validators.required, Validators.min(0)])
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: ServiceEntity,
  private restRequestService : RestRequestService<Service>,
  public dialogRef: MatDialogRef<ServiceDialog>) {
    this.serviceForm.get('name')?.setValue(data.name);
    this.serviceForm.get('price')?.setValue(data.price);
  }

  cancel(){
    this.dialogRef.close();
  }
  onSubmit(){
    if(this.serviceForm.valid){
      const service = new ServiceEntity()
      service.name = this.serviceForm.get('name')?.value!
      service.price = this.serviceForm.get('price')?.value!
      this.restRequestService.put(service, "/api/service/" + this.data.id).subscribe();
      this.dialogRef.close()
    }
  }


}