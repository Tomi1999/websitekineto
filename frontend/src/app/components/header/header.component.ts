import { Component } from '@angular/core';
import { AdminButtonService, HeaderStateService } from '../../services/header-state.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenService } from '../../services/token.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [MatToolbarModule,RouterModule,MatButtonModule,MatButton,CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {
  constructor(public headerState : HeaderStateService,public tokenService: TokenService,private router : Router, public userService: UserService, public adminButtonService : AdminButtonService) {
    this.adminButtonService.headerEvent.subscribe((visible : boolean) => this.adminButtonService.visible = visible);
   }
  clicked(){
    this.tokenService.removeToken();
    this.router.navigate(['']);
  }
}
