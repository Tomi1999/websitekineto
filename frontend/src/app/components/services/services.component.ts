import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { RestRequestService } from '../../services/rest-request.service';
import { Service } from '../../models/service';
@Component({
  selector: 'app-services',
  standalone: true,
  imports: [MatCardModule, MatTableModule,CommonModule],
  templateUrl: './services.component.html',
  styleUrl: './services.component.scss'
})
export class ServicesComponent {
  constructor(private restRequestService: RestRequestService<Service>) {}
  public services: Service[] = [];
  displayedColumns: string[] = ['name', 'price'];
  ngOnInit(): void {
    this.loadServices();
  }
  loadServices(): void {
    this.restRequestService.get('/api/service').subscribe((data) => {
      this.services = data; 
    });
  }
}
