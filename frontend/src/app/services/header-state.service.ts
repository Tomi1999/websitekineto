import { EventEmitter, Injectable, Output, output } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {

  visible: boolean = true;
  @Output() headerEvent: EventEmitter<any> = new EventEmitter();


  constructor() { }

  show() {
    this.headerEvent.emit(true);
  }
  hide() {
    this.headerEvent.emit(false);
  }

}
@Injectable({
  providedIn: 'root'
})
export class AdminButtonService {
  visible: boolean = true;
  @Output() headerEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  show() {
    this.headerEvent.emit(true);
  }
  hide() {
    this.headerEvent.emit(false);
  }
}
