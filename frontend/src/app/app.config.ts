import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';
import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { HTTP_INTERCEPTORS, HttpClientModule, provideHttpClient, withFetch, withInterceptorsFromDi } from '@angular/common/http';
import { provideAnimations } from '@angular/platform-browser/animations';
import { apiConfig } from './config/api.config';
import { environment } from '../environments/environments';
import { TokenInterceptor } from './interceptor/token.interceptor';
import { JWT_OPTIONS, JwtHelperService, JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { TokenService } from './services/token.service';
import { MAT_DATE_LOCALE, NativeDateAdapter, provideNativeDateAdapter } from '@angular/material/core';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes),provideHttpClient(withFetch()), provideClientHydration(), provideAnimationsAsync()
    ,JwtHelperService,TokenService,NativeDateAdapter,provideNativeDateAdapter(),
    provideAnimations(),
    {provide: apiConfig, useValue: environment.apiConfig}, 
    {provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    {provide: HTTP_INTERCEPTORS,useClass: TokenInterceptor,multi: true},
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    
    importProvidersFrom(
      JwtModule.forRoot({
          config: {
              tokenGetter: () => {
                return localStorage.getItem('token');
              },
              allowedDomains: ['localhost:4200']
          },
      }),
  ),
  provideHttpClient(
      withInterceptorsFromDi()
  )
  ],
};
