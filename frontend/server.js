const express = require("express");
const path = require("path");

const app = express();

// Setăm directorul în care se află fișierele build-ului Angular
const distDir = path.join(__dirname, "dist/frontend");
app.use(express.static(distDir));

// Rutează toate cererile către aplicația Angular
app.get("*", (req, res) => {
  res.sendFile(path.join(distDir, "index.html"));
});

// Pornește serverul
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Serverul rulează pe http://localhost:${PORT}`);
});
