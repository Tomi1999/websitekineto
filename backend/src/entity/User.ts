import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from "typeorm"
import { Consult } from "./Consullt"


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @OneToMany(() => Consult, (consult) => consult.user)
    @JoinColumn()
    consult: Consult[]

    @Column()
    isAdmin : boolean = false
}
