import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, CreateDateColumn, ManyToMany, ManyToOne, JoinColumn} from "typeorm";
import { User } from "./User";
import { Service } from "./Service";

@Entity()
export class Consult {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, (user) => user.consult, {cascade: true, eager:true})
    @JoinColumn()
    user: User;

    @Column()
    date: Date;

    @Column()
    service : string;
}
