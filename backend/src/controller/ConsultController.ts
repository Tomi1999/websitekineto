import { AppDataSource } from "../data-source"
import { NextFunction, Request, Response } from "express"
import * as jwt from "jsonwebtoken"
import { env } from "../env"
import { Consult } from "../entity/Consullt"

export class ConsultController {

    private consultRepository = AppDataSource.getRepository(Consult)
    userRepository: any

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const consult = await this.consultRepository.find();
            return response.status(200).json(consult);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        const consult = await this.consultRepository.findOneBy({id});

        if (!consult) {
            return response.status(404).json({ error: 'Not Found', message: 'consult not found' });
        }
        return response.status(200).json(consult);
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {

        try {
        const { user, date, service} = request.body;
        const consult = Object.assign(new Consult(), {
            user,
            date,
            service
        })
        await this.consultRepository.save(consult)  
        return response.status(200).json({message: "consult saved with succes"})
        }  
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        let consultToRemove = await this.consultRepository.findOneBy({ id })

        if (!consultToRemove) {
            return response.status(500).json({message: "this consult not exist"})
        }

        await this.consultRepository.remove(consultToRemove)

        return response.status(200).json({message :"consult was removed"})
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async futureConsults(request: Request, response: Response, next: NextFunction) {
        try {
            const currentDate = new Date();
            const consults = await this.consultRepository.find();
            
            const futureConsults = consults.filter(consult => new Date(consult.date) > currentDate);
    
            return response.status(200).json(futureConsults);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
    async getUserConsults(request: Request, response: Response, next: NextFunction) {
        try {
          const userId = request.user.userId; 
          const consults = await this.consultRepository.find({
            where: { user: { id: userId } },
            relations: ['user']
          });
    
          return response.status(200).json(consults);
        } catch (error) {
          return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
      }
    async getOneUserConsults(request: Request, response: Response, next: NextFunction){
        try {
            const userId = request.user.userId; 
            const isAdmin = request.user.isAdmin; 
            const targetUserId = parseInt(request.params.userId);
    
            let consults;
            if (isAdmin) {
                if (!targetUserId) {
                    return response.status(400).json({ message: 'User ID must be provided for admin' });
                }
                consults = await this.consultRepository.find({
                    where: { user: { id: targetUserId } },
                    relations: ['user']
                });
            } else {
                consults = await this.consultRepository.find({
                    where: { user: { id: userId } },
                    relations: ['user']
                });
            }
    
            return response.status(200).json(consults);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
    async removeMyConsult(request: Request, response: Response, next: NextFunction) {
        try {
            const userId = request.user.userId;
            const id = parseInt(request.params.id)
            let consult = await this.consultRepository.findOneBy({id})

            if (!consult) {
                return response.status(404).json({ message: 'Consultation not found' });
            }
            if (consult.user.id !== userId) {
                return response.status(403).json({ message: 'Unauthorized: You are not the owner of this consultation' });
            }
            await this.consultRepository.remove(consult);
    
            return response.status(200).json({ message: 'Consultation deleted successfully' });
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }


}