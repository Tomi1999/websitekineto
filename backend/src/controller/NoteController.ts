import { AppDataSource } from "../data-source"
import { NextFunction, Request, Response } from "express"
import { Note } from "../entity/Note"
import * as jwt from "jsonwebtoken"
import { env } from "../env"

export class NoteController {

    private noteRepository = AppDataSource.getRepository(Note)

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const notes = await this.noteRepository.find();
            return response.status(200).json(notes);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        const note = await this.noteRepository.findOneBy({id});

        if (!note) {
            return response.status(404).json({ error: 'Not Found', message: 'note not found' });
        }
        return response.status(200).json(note);
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
        const { title, message, email} = request.body;
        const note = Object.assign(new Note(), {
            title,
            message,
            email
        })
        await this.noteRepository.save(note)
        return response.status(200).json({message: "note saved with succes"})
        }  
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        let noteToRemove = await this.noteRepository.findOneBy({ id })

        if (!noteToRemove) {
            return response.status(500).json({message: "this note not exist"})
        }

        await this.noteRepository.remove(noteToRemove)

        return response.status(200).json({message :"note was removed"})
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
}