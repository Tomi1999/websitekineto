import { AppDataSource } from "../data-source"
import { NextFunction, Request, Response } from "express"
import { User } from "../entity/User"
import * as jwt from "jsonwebtoken"
import { env } from "../env"

export class UserController {

    private userRepository = AppDataSource.getRepository(User)

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const users = await this.userRepository.find();
            return response.status(200).json(users);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        const user = await this.userRepository.findOneBy({id});

        if (!user) {
            return response.status(404).json({ error: 'Not Found', message: 'User not found' });
        }
        return response.status(200).json(user);
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
        const { email, password} = request.body;
        const user = Object.assign(new User(), {
            email,
            password
        })
        await this.userRepository.save(user)
        return response.status(200).json({message: user + " saved with succes"})
        }  
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        let userToRemove = await this.userRepository.findOneBy({ id })

        if (!userToRemove) {
            return response.status(500).json({message: "this user not exist"})
        }

        await this.userRepository.remove(userToRemove)

        return response.status(200).json({message :"user " + userToRemove.email + " was removed"})
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
    async update(request: Request, response: Response, next: NextFunction){
        try {
            const id = parseInt(request.params.id)
            if(!await this.userRepository.findOneBy({id}))
                return response.status(404).json({ error: 'Not Found', message: 'User not found' });
            if(await this.userRepository.findOneBy({email: request.body.email}) != null) {
                response.json({message: 'Email already used'})
                return response.status(403).send()
            }
            
            const {email,password,isAdmin} = request.body;
            const update = Object.assign(new User(), {
                email,
                password,
                isAdmin
            })
            
            const updatedUser = await this.userRepository.update(id, {email:update.email, password :update.password, isAdmin: update.isAdmin})
            return response.status(200).json({ message: 'User updated successfully', user: updatedUser });
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
    async login(request: Request, response: Response, next: NextFunction){

        let { email, password } = request.body;
        let user : User;
        user = await this.userRepository.findOne({where: { email,password }});
        if(user) {
        const token = jwt.sign({ userId: user.id, email: user.email, isAdmin: user.isAdmin, consult: user.consult}, env.token , {expiresIn: "1h"});
        response.statusMessage = "Valid credentials"
        response.statusCode = 200
        response.json({token : token})
        } else {
            response.json({message: 'Invalid credentials'})
            response.status(401).send();
        }
    };
    async register(request: Request, response: Response, next: NextFunction){
        
        
        if(await this.userRepository.findOneBy({email: request.body.email}) != null) {
            response.json({message: 'Already registered'})
            return response.status(403).send()
        }
        const user = new User
        user.email = request.body.email;
        user.password = request.body.password;
        try {
            await this.userRepository.save(user)
            response.json({message: 'Registered with succes'})
            return response.status(200).send()
        }
        catch {
            response.json({message: 'Error while registering'})
            return response.status(500).send()
        }


    }
    async getActualUser(request: Request, response: Response, next: NextFunction){
        response.json({userId: request.user.userId, email: request.user.email,isAdmin:request.user.isAdmin});
    }

}