import { AppDataSource } from "../data-source"
import { NextFunction, Request, Response } from "express"
import { Service } from "../entity/Service"


export class ServiceController {

    private serviceRepository = AppDataSource.getRepository(Service)

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            const service = await this.serviceRepository.find();
            return response.status(200).json(service);
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        const service = await this.serviceRepository.findOneBy({id});

        if (!service) {
            return response.status(404).json({ error: 'Not Found', message: 'service not found' });
        }
        return response.status(200).json(service);
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
        const { name, price} = request.body;
        const service = Object.assign(new Service(), {
            name,
            price,
        })
        await this.serviceRepository.save(service)
        return response.status(200).json({message: "service saved with succes"})
        }  
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        let serviceToRemove = await this.serviceRepository.findOneBy({ id })

        if (!serviceToRemove) {
            return response.status(500).json({message: "this service not exist"})
        }

        await this.serviceRepository.remove(serviceToRemove)

        return response.status(200).json({message :"service was removed"})
        }
        catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
    async update(request: Request, response: Response, next: NextFunction) {
        try {
        const id = parseInt(request.params.id)
        if(!await this.serviceRepository.findOneBy({id}))
        return response.status(404).json({ error: 'Not Found', message: 'Service not found' });
                    
        const {name, price} = request.body;
        const update = Object.assign(new Service(), {
            name,
            price
        })
                    
        const updatedService = await this.serviceRepository.update(id, {name:update.name, price :update.price})
        return response.status(200).json({ message: 'Service updated successfully', user: updatedService });
        } catch (error) {
            return response.status(500).json({ error: 'Internal Server Error', message: error.message });
        }
    }
}