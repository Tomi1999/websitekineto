import "reflect-metadata"
import * as express from "express"
import * as bodyParser from "body-parser"
import { Request, Response } from "express"
import { AppDataSource } from "./data-source"
import { Routes } from "./routes"
import { User } from "./entity/User"
import { checkJwt } from "./middlewares/checkJwt";
import { checkAdmin } from "./middlewares/checkAdmin"
AppDataSource.initialize().then(async () => {

    const app = express()
    app.use(bodyParser.json())
    Routes.forEach(route => {
        const middlewares: Array<Function> = [];
        if (route.guard) {
            middlewares.push(checkJwt);
        }
        if (route.admin) {
            middlewares.push(checkAdmin);
        }
        (app as any)[route.method](route.route, ...middlewares, async (req: Request, res: Response, next: Function) => {
            try {
                await (new (route.controller as any))[route.action](req, res, next);
            } catch (error) {
                next(error);
            }
        });
    });


    app.listen(3000)


    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results")

}).catch(error => console.log(error))
