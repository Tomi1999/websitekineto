import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./entity/User"
import { Note } from "./entity/Note"
import { Consult } from "./entity/Consullt"
import { Service } from "./entity/Service"

export const AppDataSource = new DataSource({
    
    type: "postgres",
    host: "localhost",
    port: 5433,
    username: "postgres",
    password: "641e0c2c",
    database: "kineto",
    synchronize: true,
    logging: false,
    entities: [User,Note,Consult,Service],
    subscribers: [],
    migrations: [],
})
